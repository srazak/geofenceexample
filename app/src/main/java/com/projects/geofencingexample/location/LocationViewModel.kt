package com.projects.geofencingexample.location

import android.content.Context
import android.location.Location
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class LocationViewModel(context: Context) : ViewModel() {
    private val locationRepo: LocationRepo
    private var locationMutableLiveData: MutableLiveData<Location>
    val currentLocationFromViewModel: MutableLiveData<Location>
        get() {
            locationMutableLiveData = locationRepo.currentLocationFromRepo
            Log.e(TAG, "getCurrentLocationFromViewModel: Fetching Current Location")
            return locationMutableLiveData
        }
    val lastKnownLocationFromViewModel: MutableLiveData<Location>
        get() {
            Log.e(TAG, "getLastKnownLocationFromViewModel: Fetching Last Known Location")
            locationMutableLiveData = locationRepo.lastKnownLocation
            return locationMutableLiveData
        }

    companion object {
        private const val TAG = "LocationViewModel"
    }

    init {
        locationMutableLiveData = MutableLiveData()
        locationRepo = LocationRepo(context)
    }
}