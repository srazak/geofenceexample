package com.projects.geofencingexample.location

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.util.Log
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.tasks.CancellationToken
import com.google.android.gms.tasks.CancellationTokenSource
import com.google.android.gms.tasks.OnTokenCanceledListener
import com.google.android.gms.tasks.Task

class LocationRepo(context: Context) {
    private val context: Context
    private val locationMutableLiveData: MutableLiveData<Location>
    private val fusedLocationProviderClient: FusedLocationProviderClient
    val currentLocationFromRepo: MutableLiveData<Location>
        get() {
            Log.d(TAG, "getCurrentLocation: Called")
            if (ActivityCompat.checkSelfPermission(
                    context, Manifest.permission.ACCESS_FINE_LOCATION
                )
                != PackageManager.PERMISSION_GRANTED
            ) {
                Log.d(TAG, "getCurrentLocation: Permission Not Granted")
                ActivityCompat.checkSelfPermission(
                    context, Manifest.permission.ACCESS_COARSE_LOCATION
                )
            }
            fusedLocationProviderClient.getCurrentLocation(
                LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY,
                object : CancellationToken() {
                    override fun isCancellationRequested(): Boolean {
                        return false
                    }

                    override fun onCanceledRequested(onTokenCanceledListener: OnTokenCanceledListener): CancellationToken {
                        return CancellationTokenSource().token
                    }
                }).addOnCompleteListener { task: Task<Location?> ->
                task.addOnSuccessListener { location: Location? ->
                    Log.e(TAG, "Location on success : $location")
                    if (location != null) {
                        locationMutableLiveData.value = location
                        Log.e(TAG, "getCurrentLocation: $location")
                    } else Log.e(
                        TAG,
                        "getCurrentLocationFromRepo: Unable to fetch Location Reason:- Location is null"
                    )
                }
            }.addOnFailureListener { e: Exception? ->
                Log.e(TAG, "getCurrentLocation: ", e)
                locationMutableLiveData.value = null
                Toast.makeText(
                    context,
                    "Unable to Fetch Location ${e?.message}",
                    Toast.LENGTH_SHORT
                )
                    .show()
            }
            return locationMutableLiveData
        }
    val lastKnownLocation: MutableLiveData<Location>
        get() {
            Log.d(TAG, "getLastKnownLocation: Called")
            if (ActivityCompat.checkSelfPermission(
                    context, Manifest.permission.ACCESS_FINE_LOCATION
                )
                != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.checkSelfPermission(
                    context, Manifest.permission.ACCESS_COARSE_LOCATION
                )
            }
            fusedLocationProviderClient.lastLocation.addOnCompleteListener { task: Task<Location?> ->
                task.addOnSuccessListener { location: Location? ->
                    if (location != null) {
                        locationMutableLiveData.value = location
                        Log.e(TAG, "getLastKnownLocation: $location")
                    } else Log.e(TAG, "getLastKnownLocation: Unable to fetch Location")
                }
            }.addOnFailureListener { e: Exception? ->
                Log.e(TAG, "getLastKnownLocation: ", e)
                locationMutableLiveData.setValue(null)
            }
            return locationMutableLiveData
        }

    companion object {
        private const val TAG = "LocationRepo"
    }

    init {
        Log.d(TAG, "LocationRepo: Constructor")
        this.context = context
        locationMutableLiveData = MutableLiveData()
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context)
    }
}