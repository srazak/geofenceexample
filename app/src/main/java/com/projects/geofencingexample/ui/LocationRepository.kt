package com.projects.geofencingexample.ui

import com.projects.geofencingexample.api.APIServiceGenerator
import com.projects.geofencingexample.api.GeofenceAPI
import com.projects.geofencingexample.dataclass.LocationModelClass

class LocationRepository {
    suspend fun updateLocationData(locationModelClass: LocationModelClass) =
        APIServiceGenerator.createService(GeofenceAPI::class.java, "")
            .updateLocations(listOf(locationModelClass))
}