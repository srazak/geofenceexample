package com.projects.geofencingexample.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

@Suppress("UNCHECKED_CAST")
class LocationUpdateViewModelFactory(private val locationRepository: LocationRepository) :
    ViewModelProvider.Factory {


    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return LocationUpdateViewModel(locationRepository = locationRepository) as T
    }
}