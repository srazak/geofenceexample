package com.projects.geofencingexample.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.projects.geofencingexample.api.Resource
import com.projects.geofencingexample.dataclass.LocationModelClass
import com.projects.geofencingexample.dataclass.LocationUpdateResponse
import kotlinx.coroutines.launch
import retrofit2.Response

class LocationUpdateViewModel(val locationRepository: LocationRepository) : ViewModel() {
    val updateLocation: MutableLiveData<Resource<LocationUpdateResponse>> =
        MutableLiveData()

    fun getUpdateLocationResponse(locationModelClass: LocationModelClass) = viewModelScope.launch {
        updateLocation.postValue(Resource.Loading())
        val response = locationRepository.updateLocationData(locationModelClass)
        updateLocation.postValue(handleResponse(response))
    }

    private fun handleResponse(response: Response<LocationUpdateResponse>): Resource<LocationUpdateResponse> {
        if (response.isSuccessful) {
            response.body()?.let {
                return Resource.Success(it)
            }
        }
        return Resource.Failure(null, response.message() + response.code())
    }
}