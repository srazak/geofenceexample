package com.projects.geofencingexample.ui

import android.Manifest
import android.annotation.SuppressLint
import android.app.PendingIntent
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.text.TextUtils
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.location.*
import com.google.android.material.button.MaterialButton
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textview.MaterialTextView
import com.projects.driverapp.utils.Preferences
import com.projects.geofencingexample.R
import com.projects.geofencingexample.api.Resource
import com.projects.geofencingexample.dataclass.LocationModelClass
import com.projects.geofencingexample.location.GPS
import com.projects.geofencingexample.location.LocationViewModel
import com.projects.geofencingexample.location.LocationViewModelFactory
import com.projects.geofencingexample.receiver.GeofenceBroadcastReceiver
import com.projects.geofencingexample.utils.CommonUtils
import com.projects.geofencingexample.utils.Constants
import java.util.*
import kotlin.collections.ArrayList


class MainActivity : AppCompatActivity() {
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private lateinit var geofencingClient: GeofencingClient
    private var geofenceArrayList = ArrayList<Geofence>()
    private var gpsEnable = false
    private lateinit var tv: MaterialTextView
    private lateinit var etMobile: TextInputEditText
    private lateinit var btnSave: MaterialButton
    private lateinit var locationUpdateViewModel: LocationUpdateViewModel
    private val locationRepository: LocationRepository = LocationRepository()


    override fun onStart() {
        super.onStart()
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        geofencingClient = LocationServices.getGeofencingClient(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        tv = findViewById(R.id.tv_detail)
        etMobile = findViewById(R.id.et_phNo)
        btnSave = findViewById(R.id.btn_save_number)
        locationUpdateViewModel =
            ViewModelProviders.of(this, LocationUpdateViewModelFactory(locationRepository))
                .get(LocationUpdateViewModel::class.java)

        btnSave.setOnClickListener {
            if (etMobile.text != null)
                if (TextUtils.isEmpty(etMobile.text.toString())) {
                    Log.e(TAG, "onCreate: Empty Details")
                    Toast.makeText(this, "Please enter phone number", Toast.LENGTH_SHORT).show()
                } else {
                    if (etMobile.text?.length!! < 10) {
                        Toast.makeText(this, "Please enter valid phone Number", Toast.LENGTH_SHORT)
                            .show()
                    }
                    btnSave.isEnabled = false
                    Preferences.setPreference(this, Constants.PhNo, etMobile.text.toString())
                    checkAndRequestLocationPermission()
                }
            else Toast.makeText(this, "Please Enter Phone Number", Toast.LENGTH_SHORT).show()
        }
    }

    private fun checkAndRequestLocationPermission() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            fetchLocation()
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                )
            ) {
                showRationaleDialog(LOCATION_REQUEST_CODE)
            }
            ActivityCompat.requestPermissions(
                this,
                arrayOf(
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ),
                LOCATION_REQUEST_CODE
            )
        }
    }

    private fun populateGeofenceList(location: Location?) {
        geofenceArrayList.clear()
        if (location != null) {
            geofenceArrayList.add(
                Geofence.Builder()
                    // Set the request ID of the geofence. This is a string to identify this
                    // geofence.
                    .setRequestId("IntuGEOFence")

                    // Set the circular region of this geofence.
                    .setCircularRegion(location.latitude, location.longitude, 100F)

                    // Set the expiration duration of the geofence. This geofence gets automatically
                    // removed after this period of time.
                    .setExpirationDuration(Geofence.NEVER_EXPIRE)

                    // Set the transition types of interest. Alerts are only generated for these
                    // transition. We track entry and exit transitions in this sample.
                    .setTransitionTypes(
                        Geofence.GEOFENCE_TRANSITION_ENTER or
                                Geofence.GEOFENCE_TRANSITION_EXIT
                    )
                    .build()
            )
            addGeofences()
        }
    }

    /**
     * Builds and returns a GeofencingRequest. Specifies the list of geofences to be monitored.
     * Also specifies how the geofence notifications are initially triggered.
     */
    private fun getGeofencingRequest(): GeofencingRequest {
        // Return a GeofencingRequest.
        Log.e(TAG, "getGeofencingRequest: Called ")
        return GeofencingRequest.Builder().apply {
            // The INITIAL_TRIGGER_ENTER flag indicates that geofencing service should trigger a
            // GEOFENCE_TRANSITION_ENTER notification when the geofence is added and if the device
            // is already inside that geofence.
            setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER)
            // Add the geofences to be monitored by geofencing service.
            addGeofences(geofenceArrayList)
        }.build()
    }


    /**
     * Gets a PendingIntent to send with the request to add or remove Geofences. Location Services
     * issues the Intent inside this PendingIntent whenever a geofence transition occurs for the
     * current list of geofences.
     *
     * @return A PendingIntent for the IntentService that handles geofence transitions.
     */

    private val geofencePendingIntent: PendingIntent by lazy {
        Log.e(TAG, "Pending Intent Called  ")
        val intent = Intent(this, GeofenceBroadcastReceiver::class.java)
        // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when calling
        // addGeofences() and removeGeofences().
        PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
    }

    private fun addGeofences() {
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }
        geofencingClient.addGeofences(getGeofencingRequest(), geofencePendingIntent).run {
            addOnSuccessListener {
                // Geofences added
                // ...
                Log.e(TAG, "addGeofences: Geofences Added Successfully")
            }
            addOnFailureListener {
                // Failed to add geofences
                // ...
                Log.e(TAG, "addGeofences: Failed to add Geofences reason: ${it.message}", it)
            }
        }
    }

    private fun removeGeofences() {
        geofencingClient.removeGeofences(geofencePendingIntent).run {
            addOnSuccessListener {
                // Geofences removed
                // ...
                Log.e(TAG, "removeGeofences: Geofence Removed Successfully")
            }
            addOnFailureListener {
                // Failed to remove geofences
                // ...
                Log.e(TAG, "removeGeofences: Failure : ${it.message}", it)
            }
        }
    }

    private fun fetchLocation() {
        if (!gpsEnable) {
            GPS(this).turnGPSOn(object : GPS.OnGpsListener {
                override fun gpsStatus(isGPSEnable: Boolean) {
                    gpsEnable = isGPSEnable
                }
            })
        }
        val locationViewModel = ViewModelProviders.of(this, LocationViewModelFactory(this))
            .get(LocationViewModel::class.java)
        locationViewModel.currentLocationFromViewModel.observe(this, {
            if (it != null) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    if (
                        ActivityCompat.checkSelfPermission(
                            this,
                            Manifest.permission.ACCESS_BACKGROUND_LOCATION
                        ) == PackageManager.PERMISSION_GRANTED
                    ) {
                        tv.text = it.toString()
                    } else {
                        if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_BACKGROUND_LOCATION)) {
                            showRationaleDialog(
                                BACKGROUND_REQUEST_CODE
                            )
                        }
                        ActivityCompat.requestPermissions(
                            this, arrayOf(Manifest.permission.ACCESS_BACKGROUND_LOCATION),
                            BACKGROUND_REQUEST_CODE
                        )
                    }
                }
                @SuppressLint("HardwareIds") val androidId = Settings.Secure.getString(
                    contentResolver,
                    Settings.Secure.ANDROID_ID
                )
                val loc: ArrayList<Double> = ArrayList()
                loc.add(it.latitude)
                loc.add(it.longitude)

                val phNo = Preferences.getPreference(this, Constants.PhNo, "7032159853")
                val locationModelClass =
                    LocationModelClass(
                        it.altitude,
                        it.accuracy,
                        loc,
                        "Test",
                        it.bearing,
                        it.time,
                        phNo,
                        "Android", androidId, it.speed, "No Transition Details"
                    )
                if (CommonUtils.isNetworkAvailable(this)) {
                    locationUpdateViewModel.getUpdateLocationResponse(locationModelClass)
                    locationUpdateViewModel.updateLocation.observe(this, { resourceResponse ->
                        when (resourceResponse) {
                            is Resource.Success -> {
                                Log.e(TAG, "updateLocation: Success")
                                resourceResponse.data?.let { locationUpdateResponse ->
                                    Log.e(
                                        TAG,
                                        "updateLocation: ${locationUpdateResponse.message} ,Res= ${locationUpdateResponse.res}, Status = ${locationUpdateResponse.status}"
                                    )
                                }
                            }
                            is Resource.Failure -> {
                                Log.e(TAG, "updateLocation : Failure ")
                                resourceResponse.message?.let { message ->
                                    Log.e(TAG, "updateLocation: $message")
                                }
                            }
                            is Resource.Loading -> {
                                Log.e(TAG, "updateLocation: Loading ")
                            }
                        }
                    })
                } else Toast.makeText(this, "Please Check Internet Connection", Toast.LENGTH_SHORT)
                    .show()

                populateGeofenceList(it)
            } else Log.e(TAG, "fetchLocation: Unknown Error : Unable to fetch Location")
        })
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            LOCATION_REQUEST_CODE -> {
                if (grantResults.isNotEmpty()) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        Log.e(TAG, "onRequestPermissionsResult: ")
                        fetchLocation()
                    } else {
                        Log.e(TAG, "onRequestPermissionsResult:  Location Permission not granted")
                    }
                }
            }
            BACKGROUND_REQUEST_CODE -> {
                if (grantResults.isNotEmpty()) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        Log.e(TAG, "onRequestPermissionsResult: ")
                        fetchLocation()
                    } else {
                        Log.e(
                            TAG,
                            "onRequestPermissionsResult:  Background Location Permission not granted"
                        )
                    }
                }
            }
        }
    }

    companion object {
        private const val TAG = "MainActivity"
        private const val LOCATION_REQUEST_CODE = 34
        private const val BACKGROUND_REQUEST_CODE = 32
    }

//    override fun removeAndAddGeofence(location: Location, geofenceTransitionDetails: String) {
//        Log.e(
//            TAG,
//            "removeAndAddGeofence: Location is $location and the transition details are $geofenceTransitionDetails"
//        )
//        tv.append(" $location and the transition details are $geofenceTransitionDetails")
//        removeGeofences()
//        populateGeofenceList(location)
//    }

    private fun showRationaleDialog(requestCode: Int) {
        val dialog =
            MaterialAlertDialogBuilder(this)
                .setTitle("Permission Required")
                .setMessage("Please allow location all the time to continue")
                .setPositiveButton(
                    "Allow"
                ) { _, _ ->
                    Log.e(TAG, "showRationaleDialog: Button Clicked Accept")
                    when (requestCode) {
                        BACKGROUND_REQUEST_CODE -> {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                                ActivityCompat.requestPermissions(
                                    this@MainActivity,
                                    arrayOf(
                                        Manifest.permission.ACCESS_FINE_LOCATION,
                                        Manifest.permission.ACCESS_COARSE_LOCATION,
                                        Manifest.permission.ACCESS_BACKGROUND_LOCATION
                                    ),
                                    BACKGROUND_REQUEST_CODE
                                )
                            }
                        }
                        LOCATION_REQUEST_CODE -> {
                            ActivityCompat.requestPermissions(
                                this@MainActivity,
                                arrayOf(
                                    Manifest.permission.ACCESS_FINE_LOCATION,
                                    Manifest.permission.ACCESS_COARSE_LOCATION
                                ),
                                LOCATION_REQUEST_CODE
                            )
                        }
                    }
                }
                .setNegativeButton(
                    "No"
                ) { dialog, _ -> dialog?.dismiss() }
                .setCancelable(false)
                .create()
        dialog.show()
    }


}