package com.projects.geofencingexample.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;

import com.projects.geofencingexample.R;

import java.util.Objects;

public class ProgressDialog {
    private static Dialog progressDialog;

    public static void start(Context context) {
        if (!isShowing()) {
            if (!((Activity) context).isFinishing()) {
                progressDialog = new Dialog(context);
                progressDialog.setCancelable(false);
                Objects.requireNonNull(progressDialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                progressDialog.setContentView(R.layout.progess);
                progressDialog.show();
            }
        }
    }

    public static void dismiss() {
        try {
            if ((progressDialog != null) && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        } catch (final Exception e) {
            // Handle or log or ignore
        } finally {
            progressDialog = null;
        }
    }

    public static boolean isShowing() {
        if (progressDialog != null) {
            return progressDialog.isShowing();
        } else {
            return false;
        }
    }


}
