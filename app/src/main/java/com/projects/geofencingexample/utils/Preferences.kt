package com.projects.driverapp.utils

import android.content.Context
import android.content.SharedPreferences
import java.util.*

object Preferences {
    private const val TAG = "Prefrences"

    /**
     * @param context - pass context
     * @return SharedPreferences
     */
    private fun getSharedPreferences(context: Context): SharedPreferences {
        return context.getSharedPreferences(TAG, Context.MODE_PRIVATE)
    }

    /**
     * @param context - context
     * @param key     - Constant key, will be used for accessing the stored value
     * @param val     - String value to be stored
     */
    fun setPreference(context: Context, key: String?, `val`: String?) {
        val settings = getSharedPreferences(context)
        val editor = settings.edit()
        editor.putString(key, `val`)
        editor.apply()
    }

    /**
     * @param context - context
     * @param key     - Constant key, will be used for accessing the stored value
     * @param val     - String value to be stored
     */
    fun setPreference_float(
        context: Context, key: String?,
        `val`: Float
    ) {
        val settings = getSharedPreferences(context)
        val editor = settings.edit()
        editor.putFloat(key, `val`)
        editor.apply()
    }

    @JvmStatic
    fun setPreference(context: Context, key: String?, `val`: Boolean) {
        val settings = getSharedPreferences(context)
        val editor = settings.edit()
        editor.putBoolean(key, `val`)
        editor.apply()
    }

    fun setPreference_int(context: Context, key: String?, `val`: Int) {
        val settings = getSharedPreferences(context)
        val editor = settings.edit()
        editor.putInt(key, `val`)
        editor.apply()
    }

    /**
     * Add preferences
     *
     * @param context - context
     * @param key     - Constant key, will be used for accessing the stored value
     * @param val     - long value to be stored, mostly used to store FB Session
     * value
     */
    fun setPreference_long(context: Context, key: String?, `val`: Long) {
        val settings = getSharedPreferences(context)
        val editor = settings.edit()
        editor.putLong(key, `val`)
        editor.apply()
    }

    /**
     * Add preferences
     *
     * @param key - Constant key, will be used for accessing the stored value
     */
    fun setPreferenceArray(
        mContext: Context, key: String,
        array: ArrayList<String?>
    ): Boolean {
        val prefs = getSharedPreferences(mContext)
        val editor = prefs.edit()
        editor.putInt(key + "_size", array.size)
        for (i in array.indices) editor.putString(key + "_" + i, array[i])
        return editor.commit()
    }

    fun clearPreferenceArray(c: Context, key: String) {
        val settings = getSharedPreferences(c)
        getPreferenceArray(c, key)
        if (getPreferenceArray(c, key).size > 0) {
            for (element in getPreferenceArray(c, key)) {
                if (findPreferenceKey(c, element) != null
                    && settings.contains(findPreferenceKey(c, element))
                ) {
                    val editor = settings.edit()
                    editor.remove(findPreferenceKey(c, element))
                    editor.apply()
                }
            }
        }
    }

    private fun findPreferenceKey(con: Context, value: String?): String? {
        val settings = getSharedPreferences(con)
        val editor = settings.all
        for ((key, value1) in editor) {
            if (value == value1) {
                return key
            }
        }
        return null // not found
    }
    /*
     * Remove preference key
     *
     * @param context
     *            - context
     * @param key
     *            - the key which you stored before
     */
    // public static void removePreference(Context context, String key) {
    // SharedPreferences settings = Preferences.getSharedPreferences(context);
    // SharedPreferences.Editor editor = settings.edit();
    // editor.remove(key);
    // editor.commit();
    // }
    /**
     * Get preference value by passing related key
     *
     * @param context - context
     * @param key     - key value used when adding preference
     * @return - String value
     */
    @JvmStatic
    fun getPreference(context: Context, key: String?, defValue: String?): String? {
        val prefs = getSharedPreferences(context)
        return prefs.getString(key, defValue)
    }

    /**
     * Get preference ArrayList<String> value by passing related key
     *
     * @param key - key value used when adding preference
     * @return - ArrayList<String> value
    </String></String> */
    private fun getPreferenceArray(
        mContext: Context,
        key: String
    ): ArrayList<String?> {
        val prefs = getSharedPreferences(mContext)
        val size = prefs.getInt(key + "_size", 0)
        val array = ArrayList<String?>(size)
        for (i in 0 until size) array.add(prefs.getString(key + "_" + i, null))
        return array
    }

    /**
     * Get preference value by passing related key
     *
     * @param context - context
     * @param key     - key value used when adding preference
     * @return - long value
     */
    fun getPreference_long(context: Context, key: String?): Long {
        val prefs = getSharedPreferences(context)
        return prefs.getLong(key, 0)
    }

    fun getPreference_boolean(context: Context, key: String?): Boolean {
        val prefs = getSharedPreferences(context)
        return prefs.getBoolean(key, false)
    }

    fun getPreference_int(context: Context, key: String?): Int {
        val prefs = getSharedPreferences(context)
        return prefs.getInt(key, 111)
    }
    /*
     * Clear all stored preferences
     *
     * @param context
     *            - context
     */
    // public static void removeAllPreference(Context context) {
    // SharedPreferences settings = Preferences.getSharedPreferences(context);
    // SharedPreferences.Editor editor = settings.edit();
    // editor.clear();
    // editor.commit();
    // }
    /**
     * Clear all stored preferences
     *
     * @param context - context
     */
    fun getAllPreference(context: Context): String {
        val settings = getSharedPreferences(context)
        val editor = settings.all
        val text = StringBuilder()
        try {
            for ((key, value1) in editor) {
                val value = value1!!
                // do stuff
                text.append("\t").append(key).append(" = ").append(value).append("\t")
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return text.toString()
    }

    /**
     * Remove preference key
     *
     * @param context - context
     * @param key     - the key which you stored before
     */
    fun removePreference(context: Context, key: String?) {
        val settings = getSharedPreferences(context)
        val editor = settings.edit()
        editor.remove(key)
        editor.apply()
    }

    /**
     * Clear all stored preferences
     *
     * @param context - context
     */
    fun removeAllPreference(context: Context) {
        val settings = getSharedPreferences(context)
        val editor = settings.edit()
        editor.clear()
        editor.apply()
    }
}