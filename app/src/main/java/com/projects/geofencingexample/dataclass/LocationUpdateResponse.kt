package com.projects.geofencingexample.dataclass


import com.google.gson.annotations.SerializedName

data class LocationUpdateResponse(
    @SerializedName("message")
    val message: String,
    @SerializedName("res")
    val res: Int,
    @SerializedName("result")
    val result: Result,
    @SerializedName("status")
    val status: Boolean
)

class Result