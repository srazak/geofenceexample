package com.projects.geofencingexample.dataclass

import com.google.gson.annotations.SerializedName

data class LocationModelClass(
    @SerializedName("altitude")
    val altitude: Double = 0.0,
    @SerializedName("accuracy")
    val accuracy: Float = 0.0f,
    @SerializedName("loc")
    val loc: ArrayList<Double> = ArrayList(),
    @SerializedName("name")
    val name: String = "Test1",
    @SerializedName("bearing")
    val bearing: Float = 0.0f,
    @SerializedName("timestamp")
    val timeStamp: Long = 0,
    @SerializedName("mobile")
    val mobile: String? = "",
    @SerializedName("hardware_type")
    val hardware_type: String = "Android",
    @SerializedName("device_id")
    val device_id: String = "Unknown",
    @SerializedName("speed")
    val speed: Float = 0.0f,
    @SerializedName("transitionDetails")
    val transitionDetails: String = ""
)
