package com.projects.geofencingexample.api

import android.text.TextUtils
import android.util.Log
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.projects.geofencingexample.utils.Constants.BASE_URL
import com.projects.geofencingexample.utils.Constants.TOKEN
import okhttp3.Credentials.basic
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object APIServiceGenerator {
    private const val TAG = "APIServiceGenerator"

    private val gson: Gson =
        GsonBuilder().enableComplexMapKeySerialization().serializeNulls().setPrettyPrinting()
            .setVersion(1.0).create()

    private val headers: HashMap<String?, String?> = HashMap()

    private lateinit var okHttpClient: OkHttpClient

    private fun getHeaders(): HashMap<String?, String?> {
        return headers
    }

    fun addHeader(key: String?, value: String?) {
        headers[key] = value
    }

    fun <S> createService(
        serviceClass: Class<S>, username: String, password: String
    ): S {
        return if (!TextUtils.isEmpty(username)
            && !TextUtils.isEmpty(password)
        ) {
            val authToken = basic(username, password)
            createService(serviceClass, authToken)
        } else createService(serviceClass, "")
    }

    fun <S> createService(
        serviceClass: Class<S>?, authToken: String
    ): S {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        val httpClientBuilder = OkHttpClient.Builder()
        if (authToken != TOKEN || !TextUtils.isEmpty(authToken)) {
            val interceptor = AuthenticationInterceptor(authToken)
            Log.e(
                TAG,
                "createService: Auth Token Not empty = $authToken"
            )
            if (!httpClientBuilder.interceptors().contains(interceptor)) {
                Log.e(TAG, "createService: httpClient adding Auth Interceptor")
                httpClientBuilder.addInterceptor(interceptor)
            }
        } else {
            Log.e(
                TAG,
                "createService: empty authToken or token value is $authToken"
            )
        }
        if (!httpClientBuilder.interceptors().contains(httpLoggingInterceptor)) {
            Log.e(TAG, "createService: httpClient adding httpLoggingInterceptor")
            httpClientBuilder.addInterceptor(httpLoggingInterceptor)
        }
        httpClientBuilder.connectTimeout(100, TimeUnit.SECONDS)
        httpClientBuilder.readTimeout(100, TimeUnit.SECONDS)
        httpClientBuilder.addNetworkInterceptor(Interceptor { chain: Interceptor.Chain ->
            val request: Request.Builder = chain.request().newBuilder()
            val headers =
                getHeaders()
            for (key in headers.keys) {
                val value = headers[key]
                if (value != null) {
                    if (key != null) {
                        request.addHeader(key, value)
                    }
                }
            }
            request.method(chain.request().method, chain.request().body)
            chain.proceed(request.build())
        })
        okHttpClient = httpClientBuilder.build()
        val builder = Retrofit.Builder().baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson)).client(
                okHttpClient
            )

        val retrofit: Retrofit = builder.build()
        return retrofit.create(serviceClass)
    }

}