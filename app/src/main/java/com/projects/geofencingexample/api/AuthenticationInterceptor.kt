package com.projects.geofencingexample.api

import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

class AuthenticationInterceptor(private val authToken: String) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {

        val original = chain.request()

        val requestBuilder: Request.Builder =
            original.newBuilder().header("Authorization", authToken)

        return chain.proceed(requestBuilder.build())
    }
}