package com.projects.geofencingexample.api

import com.projects.geofencingexample.dataclass.LocationModelClass
import com.projects.geofencingexample.dataclass.LocationUpdateResponse
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface GeofenceAPI {
    @POST("pings/app_tracking")
    suspend fun updateLocations(@Body locationModelClassList: List<LocationModelClass>): Response<LocationUpdateResponse>
}