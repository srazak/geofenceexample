package com.projects.geofencingexample.receiver

import android.annotation.SuppressLint
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.TaskStackBuilder
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.location.Location
import android.os.Build
import android.provider.Settings
import android.text.TextUtils
import android.util.Log
import androidx.core.app.NotificationCompat
import com.google.android.gms.location.*
import com.google.gson.Gson
import com.projects.driverapp.utils.Preferences
import com.projects.geofencingexample.R
import com.projects.geofencingexample.api.APIServiceGenerator
import com.projects.geofencingexample.api.GeofenceAPI
import com.projects.geofencingexample.dataclass.LocationModelClass
import com.projects.geofencingexample.ui.MainActivity
import com.projects.geofencingexample.utils.CommonUtils
import com.projects.geofencingexample.utils.Constants
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class GeofenceBroadcastReceiver :
    BroadcastReceiver() {
    private lateinit var geofencingClient: GeofencingClient
    private var geofenceArrayList: ArrayList<Geofence> = ArrayList()
    private lateinit var geofencePendingIntent: PendingIntent

    override fun onReceive(context: Context, intent: Intent) {
        Log.e(TAG, "onReceive: Called ")
        val geofencingEvent = GeofencingEvent.fromIntent(intent)
        geofencingClient = LocationServices.getGeofencingClient(context)
        if (geofencingEvent.hasError()) {
            val errorMessage = GeofenceStatusCodes
                .getStatusCodeString(geofencingEvent.errorCode)
            Log.e(TAG, errorMessage)
            return
        }

        geofencePendingIntent = getPendingIntent(context)

        // Get the transition type.
        val geofenceTransition = geofencingEvent.geofenceTransition

        // Test that the reported transition was of interest.
        if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER ||
            geofenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT
        ) {
            Log.e(TAG, "onReceive: Event Occured $geofenceTransition")
            // Get the geofences that were triggered. A single event can trigger
            // multiple geofences.
            val triggeringGeofences = geofencingEvent.triggeringGeofences

            // Get the transition details as a String.
            val geofenceTransitionDetails = getGeofenceTransitionDetails(
                geofenceTransition,
                triggeringGeofences
            )
            if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT) {
                removeGeofences()

                CoroutineScope(Dispatchers.IO).launch {
                    updateData(
                        geofenceTransitionDetails,
                        geofencingEvent.triggeringLocation,
                        context
                    )
                }

                populateGeofenceList(geofencingEvent.triggeringLocation)
            }
//            geofenceCallback.removeAndAddGeofence(
//                geofencingEvent.triggeringLocation,
//                geofenceTransitionDetails
//            )

            // Send notification and log the transition details.
            sendNotification(geofenceTransitionDetails, context)
            Log.i(TAG, geofenceTransitionDetails)
        } else {
            // Log the error.
            Log.e(
                TAG, "Geofence Transition invalid type $geofenceTransition"
            )
        }

    }

    private suspend fun updateData(
        geofenceTransitionDetails: String,
        triggeringLocation: Location,
        context: Context
    ) {
        @SuppressLint("HardwareIds") val androidId = Settings.Secure.getString(
            context.contentResolver,
            Settings.Secure.ANDROID_ID
        )
        val loc: ArrayList<Double> = ArrayList()
        loc.add(triggeringLocation.latitude)
        loc.add(triggeringLocation.longitude)
        val phNo = Preferences.getPreference(context, Constants.PhNo, "7032159853")
        val locationModelClass =
            LocationModelClass(
                triggeringLocation.altitude,
                triggeringLocation.accuracy,
                loc,
                "Test",
                triggeringLocation.bearing,
                triggeringLocation.time,
                phNo,
                "Android", androidId, triggeringLocation.speed, geofenceTransitionDetails
            )
        if (CommonUtils.isNetworkAvailable(context)) {
            val locationUpdateResponse =
                APIServiceGenerator.createService(GeofenceAPI::class.java, "")
                    .updateLocations(listOf(locationModelClass))
            if (locationUpdateResponse.isSuccessful) {
                locationUpdateResponse.body()?.let {
                    Log.e(
                        TAG,
                        "updateData: Success Message= ${it.message} ,status= ${it.status}, Res= ${it.res}"
                    )
                }
            } else {
                Log.e(
                    TAG,
                    "updateData: Response Failed code: ${locationUpdateResponse.code()}, Error Body --${
                        Gson().toJson(
                            locationUpdateResponse.errorBody()
                        )
                    }"
                )
            }
        } else Log.e(TAG, "updateData: No Internet Connection")


    }

    private fun getPendingIntent(context: Context): PendingIntent {
        val geofencePendingIntent: PendingIntent by lazy {
            Log.e(TAG, "Pending Intent Called  ")
            val i = Intent(context, GeofenceBroadcastReceiver::class.java)
            // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when calling
            // addGeofences() and removeGeofences().
            PendingIntent.getBroadcast(context, 0, i, PendingIntent.FLAG_UPDATE_CURRENT)
        }
        return geofencePendingIntent
    }

    /**
     * Maps geofence transition types to their human-readable equivalents.
     *
     * @param transitionType A transition type constant defined in Geofence
     * @return A String indicating the type of transition
     */
    private fun getTransitionString(transitionType: Int): String {
        return when (transitionType) {
            Geofence.GEOFENCE_TRANSITION_ENTER -> "Geofence Transition entered"
            Geofence.GEOFENCE_TRANSITION_EXIT -> "Geofence Transition exit"
            else -> "Unknown Geofence Transition"
        }
    }

    /**
     * Posts a notification in the notification bar when a transition is detected.
     * If the user clicks the notification, control goes to the MainActivity.
     */
    private fun sendNotification(notificationDetails: String, context: Context) {
        // Get an instance of the Notification manager
        val mNotificationManager =
            context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        // Android O requires a Notification Channel.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name: CharSequence = context.getString(R.string.app_name)
            // Create the channel for the notification
            val mChannel = NotificationChannel(
                CHANNEL_ID,
                name,
                NotificationManager.IMPORTANCE_DEFAULT
            )

            // Set the Notification Channel for the Notification Manager.
            mNotificationManager.createNotificationChannel(mChannel)
        }

        // Create an explicit content Intent that starts the main Activity.
        val notificationIntent = Intent(context, MainActivity::class.java)

        // Construct a task stack.
        val stackBuilder = TaskStackBuilder.create(context)

        // Add the main Activity to the task stack as the parent.
        stackBuilder.addParentStack(MainActivity::class.java)

        // Push the content Intent onto the stack.
        stackBuilder.addNextIntent(notificationIntent)

        // Get a PendingIntent containing the entire back stack.
        val notificationPendingIntent: PendingIntent =
            stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)

        // Get a notification builder that's compatible with platform versions >= 4
        val builder = NotificationCompat.Builder(context, CHANNEL_ID)

        // Define the notification settings.
        builder.setSmallIcon(R.drawable.ic_launcher_foreground) // In a real app, you may want to use a library like Volley
            // to decode the Bitmap.
            .setLargeIcon(
                BitmapFactory.decodeResource(
                    context.resources,
                    R.drawable.ic_launcher_background
                )
            )
            .setColor(Color.RED)
            .setContentTitle(notificationDetails)
            .setContentText("Transition Notification")
            .setContentIntent(notificationPendingIntent)

        // Set the Channel ID for Android O.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder.setChannelId(CHANNEL_ID) // Channel ID
        }

        // Dismiss notification once the user touches it.
        builder.setAutoCancel(true)

        // Issue the notification
        mNotificationManager.notify(0, builder.build())
    }

    private fun populateGeofenceList(location: Location?) {
        geofenceArrayList.clear()
        if (location != null) {
            Log.e(TAG, "populateGeofenceList: $location")
            geofenceArrayList.add(
                Geofence.Builder()
                    // Set the request ID of the geofence. This is a string to identify this
                    // geofence.
                    .setRequestId("IntuGEOFence")

                    // Set the circular region of this geofence.
                    .setCircularRegion(location.latitude, location.longitude, 100F)

                    // Set the expiration duration of the geofence. This geofence gets automatically
                    // removed after this period of time.
                    .setExpirationDuration(Geofence.NEVER_EXPIRE)

                    // Set the transition types of interest. Alerts are only generated for these
                    // transition. We track entry and exit transitions in this sample.
                    .setTransitionTypes(
                        Geofence.GEOFENCE_TRANSITION_ENTER or
                                Geofence.GEOFENCE_TRANSITION_EXIT
                    )
                    .build()
            )
            addGeofences()
        }
    }

    /**
     * Builds and returns a GeofencingRequest. Specifies the list of geofences to be monitored.
     * Also specifies how the geofence notifications are initially triggered.
     */
    private fun getGeofencingRequest(): GeofencingRequest {
        // Return a GeofencingRequest.
        Log.e(TAG, "getGeofencingRequest: Called ")
        return GeofencingRequest.Builder().apply {
            // The INITIAL_TRIGGER_ENTER flag indicates that geofencing service should trigger a
            // GEOFENCE_TRANSITION_ENTER notification when the geofence is added and if the device
            // is already inside that geofence.
            setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER)
            // Add the geofences to be monitored by geofencing service.
            addGeofences(geofenceArrayList)
        }.build()
    }


    /**
     * Gets a PendingIntent to send with the request to add or remove Geofences. Location Services
     * issues the Intent inside this PendingIntent whenever a geofence transition occurs for the
     * current list of geofences.
     *
     * @return A PendingIntent for the IntentService that handles geofence transitions.
     */


    @SuppressLint("MissingPermission")
    private fun addGeofences() {
        geofencingClient.addGeofences(getGeofencingRequest(), geofencePendingIntent).run {
            addOnSuccessListener {
                // Geofences added
                // ...
                Log.e(TAG, "addGeofences: Geofences Added Successfully")
            }
            addOnFailureListener {
                // Failed to add geofences
                // ...
                Log.e(
                    TAG,
                    "addGeofences: Failed to add Geofences reason: ${it.message}",
                    it
                )
            }
        }
    }

    private fun removeGeofences() {
        geofencingClient.removeGeofences(geofencePendingIntent).run {
            addOnSuccessListener {
                // Geofences removed
                // ...
                Log.e(TAG, "removeGeofences: Geofence Removed Successfully")
            }
            addOnFailureListener {
                // Failed to remove geofences
                // ...
                Log.e(TAG, "removeGeofences: Failure : ${it.message}", it)
            }
        }
    }

    /**
     * Gets transition details and returns them as a formatted string.
     *
     * @param geofenceTransition  The ID of the geofence transition.
     * @param triggeringGeofences The geofence(s) triggered.
     * @return The transition details formatted as String.
     */
    private fun getGeofenceTransitionDetails(
        geofenceTransition: Int,
        triggeringGeofences: List<Geofence>
    ): String {
        val geofenceTransitionString = getTransitionString(geofenceTransition)

        // Get the Ids of each geofence that was triggered.
        val triggeringGeofencesIdsList = ArrayList<String>()
        for (geofence in triggeringGeofences) {
            triggeringGeofencesIdsList.add(geofence.requestId)
        }
        val triggeringGeofencesIdsString = TextUtils.join(", ", triggeringGeofencesIdsList)
        return "$geofenceTransitionString: $triggeringGeofencesIdsString"
    }

    companion object {
        private const val TAG = "GeofenceBroadReceiver"
        private const val CHANNEL_ID = "channel_01"
    }
}